package com.example.restServices;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = HelloworldRun.class)
@AutoConfigureMockMvc
class ServiceControllerMockMvcTest {

    @Autowired
    private MockMvc mockMvc;

    /*
    private WebApplicationContext wac;

    ServiceControllerMockMvcTest(WebApplicationContext wac) {
        this.wac = wac;
    }

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(this.wac).build();
    }
    */

    @Test
    public void testSimpleSayHello() throws Exception {
        mockMvc.perform(get("/hello")).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testParamsSayHello() throws Exception {

        mockMvc.perform(get("/hello").param("name", "Hatake")).andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testNoParamsSayHello() throws Exception {
        String paramsUrl = UriComponentsBuilder
                .fromPath("/hello")
                .build()
                .toString();

        mockMvc.perform(get(paramsUrl)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Helloo World!!"));

    }

    @Test
    public void testObtaindetails() throws Exception {
        String name = "user";
        String output = String.format("Hello %s", name);
        mockMvc.perform(post("/details").contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(content().string(output))
                .andExpect(status().is2xxSuccessful());
    }
}