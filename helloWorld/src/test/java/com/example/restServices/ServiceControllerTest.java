package com.example.restServices;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ServiceControllerTest {

    @Autowired
    private ServiceController controller;

    @Test
    void testsayHello() {
        String output = controller.sayHello("username");
        assertEquals("Helloo username!!", output);
    }

    @Test
    void obtainDetails() {
        Details details = new Details();
        details.setName("Sakura");
        details.setAge(18);
        String response = controller.obtainDetails(details);
        assertEquals("Heyy Sakura", response);
    }

    @Test
    void helloWorld() {
        String response = controller.helloWorld();
        assertEquals("Hello World!!", response);
    }

}