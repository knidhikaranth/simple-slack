package com.example.restServices;

public class UrlConfigChallenge {
    public String getToken() {
        return token;
    }

    private String token;

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    private String challenge;
    private String type;
}
