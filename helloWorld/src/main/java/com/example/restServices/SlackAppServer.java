package com.example.restServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlackAppServer{
    public static void main(String args[]) throws Exception{
        SpringApplication.run(SlackAppServer.class, args);
    }
}
