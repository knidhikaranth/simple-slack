package com.example.restServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class HelloworldRun extends SpringBootServletInitializer {
    public static void main(String args[]){
        SpringApplication.run(HelloworldRun.class, args);
    }
}
