package com.example.restServices;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slack.api.Slack;
import com.slack.api.app_backend.events.payload.AppMentionPayload;
import com.slack.api.bolt.AppConfig;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/*
{event_id=Ev01LP066744, authorizations=[{enterprise_id=null, team_id=T01LGPLJ8AG, user_id=U01LPAYF6TB, is_bot=true, is_enterprise_install=false}], api_app_id=A01LP8HNVLH, team_id=T01LGPLJ8AG, event={client_msg_id=78508a9f-c261-4ad5-aba1-99d1835862bb, type=app_mention, text=<@U01LPAYF6TB> hii, user=U01L3RB1XGE, ts=1612158959.000600, team=T01LGPLJ8AG, blocks=[{type=rich_text, block_id=ecE8, elements=[{type=rich_text_section, elements=[{type=user, user_id=U01LPAYF6TB}, {type=text, text= hii}]}]}], channel=C01LAJX15PU, event_ts=1612158959.000600}, type=event_callback, event_context=1-app_mention-T01LGPLJ8AG-C01LAJX15PU, is_ext_shared_channel=false, event_time=1.612158959E9, token=OyRSr35kHjFlhKzABkYHMYK2}
*/

@RestController
public class SlackController {

    String SLACK_TOKEN = "OyRSr35kHjFlhKzABkYHMYK2";
    String CLIENT_ID = "1696802620356.1703289777697";
    String CLIENT_SECRET = "eed1784fa8f85c83cd49b404ba9534f5";
    String BOT_USER_OAUTH_TOKEN= "xoxb-1696802620356-1703372516929-5s6VYBqz3W1gW5aTZ3JzCCQH";
    String SIGNED_SECRET = "387ef552cfe440ada07e1fe09fd249cd";


    /*@RequestMapping(method = RequestMethod.POST, value = "/slack/events")
    public String setChallenge(@RequestBody UrlConfigChallenge reqBody) throws IOException{
        String challenge = reqBody.getChallenge();
        return challenge;
    }*/

    @RequestMapping(method = RequestMethod.POST, value = "/slack/command", consumes = "application/x-www-form-urlencoded")
    public HashMap<String, String> helloCommand(@RequestParam Map<String, Object> payload) {
        //App app = new App();
        //app.command("/hello", (req, ctx) -> ctx.ack("Hi there!"));
        //return app;

        HashMap<String, String> response = new HashMap<>();
        System.out.println(payload);
        response.put("text", ":wave: Hii there!!");
        response.put("response_type", "in_channel");
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/slack/events")
    @ResponseBody
    public JSONObject eventHandler(@RequestBody String reqBody, @RequestHeader("X-Slack-Signature") String reqSign, @RequestHeader("X-Slack-Request-Timestamp") String timestamp) throws ParseException, IOException, SlackApiException, NoSuchAlgorithmException, InvalidKeyException {

        JSONObject returnJSON = new JSONObject();
        String concatString = "v0:"+timestamp+":"+reqBody;
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(SIGNED_SECRET.getBytes(StandardCharsets.UTF_8), "HmacSHA246");
        sha256_HMAC.init(secret_key);
        byte[] rawHmac = sha256_HMAC.doFinal(concatString.getBytes("UTF-8"));
        byte[] hexArr = {(byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5', (byte)'6', (byte)'7', (byte)'8', (byte)'9', (byte)'a', (byte)'b', (byte)'c', (byte)'d', (byte)'e', (byte)'f'};
        byte[] hexChars = new byte[rawHmac.length*2];
        for(int j = 0; j < rawHmac.length; j++){
            int v = rawHmac[j] & 0xFF;
            hexChars[j*2] = hexArr[v >>> 4];
            hexChars[j*2+1] = hexArr[v & 0x0F];
        }
        String hexString = new String(hexChars);
        String calculatedSign = "v0="+hexString;

        System.out.println(reqSign);
        System.out.println(calculatedSign);

        if(!calculatedSign.equals(reqSign)){
            returnJSON.put("msg","Invalid Auth");
            return returnJSON;
        }

        Map<String, Object> payload = new Gson().fromJson(
                reqBody, new TypeToken<HashMap<String, Object>>() {
                }.getType()
        );

        String challenge = (String) payload.get("challenge");
        String type = (String) payload.get("type");

        if (type.equals("url_verification")) {
            returnJSON.put("challenge", challenge);
            return returnJSON;
        }

        String token = (String) payload.get("token");

        if (!token.equals(SLACK_TOKEN)) {
            //dont process
            System.err.println("Invalid event source");
            return returnJSON;
        }

        String event = payload.get("event").toString();
        //System.out.println(payload.get("event").getClass().getName());

        event = event.substring(1, event.length() - 1);
        Map<String, String> eventDetails = new HashMap<>();
        String parts[] = event.split(",");
        for (String pair : parts) {
            String[] keyvalue = pair.split("=");
            eventDetails.put(keyvalue[0].trim(), keyvalue[1].trim());
        }

        String eventType = (String) eventDetails.get("type");
        String text = (String) eventDetails.get("text");
        String userId = (String) eventDetails.get("user");
        String channel = (String) eventDetails.get("channel");

        System.out.println(payload);

        if (eventType.equals("app_mention")) {

            Slack slack = Slack.getInstance();
            MethodsClient methods = slack.methods(BOT_USER_OAUTH_TOKEN);
            ChatPostMessageRequest res;

            if(text.startsWith("hi")) {
                res = ChatPostMessageRequest.builder()
                        .channel(channel).text(":wave: Helloo..").build();
                ChatPostMessageResponse response = methods.chatPostMessage(res);
            }


        }
        return returnJSON;
    }
}
