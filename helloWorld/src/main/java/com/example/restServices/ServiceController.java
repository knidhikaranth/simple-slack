package com.example.restServices;

import com.slack.api.Slack;
import com.slack.api.bolt.App;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ServiceController {

    String SLACK_TOKEN = "387ef552cfe440ada07e1fe09fd249cd";

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public @ResponseBody String helloWorld(){
        return "Hello World!!";
    }

    @RequestMapping(method= RequestMethod.GET, value = "/hello")
    public String sayHello(@RequestParam(value = "name", defaultValue = "World") String name) {
        String ans = String.format("Helloo %s!!", name);
        return ans;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/details")
    public String obtainDetails(@RequestBody Details details) {
        String username = details.getName();
        int userage = details.getAge();
        String ans = String.format("Heyy %s", username);
        return ans;
    }

}